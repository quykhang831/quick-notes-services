package aavn.notes.boundary;

import aavn.notes.model.Note;
import aavn.notes.service.NoteService;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.annotation.security.PermitAll;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HEAD;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.PATCH;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@ApplicationScoped
public class NoteResource implements PanacheRepository<Note> {
    private final NoteService noteService;

    @Inject
    public NoteResource(NoteService noteService) {
        this.noteService = noteService;
    }

    @HEAD
    @Produces(MediaType.TEXT_PLAIN)
    @PermitAll
    public String ping() {
        return "alive";
    }

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllNotes() {
        return Response
                .ok()
                .entity(noteService.getAllNotes())
                .build();
    }

    @POST
    @Path("/")
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addNote(String bodyText) {
        return Response
                .status(Response.Status.CREATED)
                .entity(noteService.addNote(bodyText))
                .build();
    }

    @PATCH
    @Path("/{noteId}")
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateNote(@PathParam("noteId") Long noteId, String bodyText) {
        try {
            Note note = noteService.updateNote(noteId, bodyText);
            return Response.ok().entity(note).build();
        } catch (NotFoundException e) {
            throw new WebApplicationException(Response.status(404).entity(e.getMessage()).build());
        }
    }

    @DELETE
    @Path("/{noteId}")
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteNoteById(@PathParam("noteId") Long noteId) {
        try {
            noteService.deleteNoteById(noteId);
            return Response
                    .status(Response.Status.ACCEPTED)
                    .entity("Note " + noteId + " was deleted successfully")
                    .build();

        } catch (NotFoundException e) {
            throw new WebApplicationException(Response.status(404).entity(e.getMessage()).build());
        }
    }
}
