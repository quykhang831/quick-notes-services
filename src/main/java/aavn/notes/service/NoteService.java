package aavn.notes.service;

import aavn.notes.model.Note;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.NotFoundException;

import java.util.Date;
import java.util.List;

@ApplicationScoped
public class NoteService implements PanacheRepository<Note> {

    public List<Note> getAllNotes() {
        return listAll();
    }

    public Note getNoteById(Long id) {
        return findByIdOptional(id).orElseThrow(() -> new NotFoundException("Can not find the Note with id: " + id));
    }

    public Note addNote(String bodyText) {
        Note note = new Note();
        note.setBodyText(bodyText);
        note.setCreatedDate(new Date());
        persist(note);
        return note;
    }

    public Note updateNote(Long id, String bodyText) {
        Note oldNote = getNoteById(id);
        if (bodyText != null) {
            oldNote.setBodyText(bodyText);
        }
        persist(oldNote);
        return oldNote;
    }

    public void deleteNoteById(Long id) {
        Note note = getNoteById(id);
        delete(note);
    }
}
